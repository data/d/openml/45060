# OpenML dataset: online_shoppers

https://www.openml.org/d/45060

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The dataset consists of feature vectors belonging to 12,330 sessions.The dataset was formed so that each sessionwould belong to a different user in a 1-year period to avoidany tendency to a specific campaign, special day, userprofile, or period.Source: https://archive.ics.uci.edu/ml/datasets/Online+Shoppers+Purchasing+Intention+Dataset

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45060) of an [OpenML dataset](https://www.openml.org/d/45060). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45060/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45060/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45060/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

